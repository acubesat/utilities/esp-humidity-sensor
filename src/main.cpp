/* Started with Basic MQTT example */

#include <ESP8266WiFi.h>      //ESP library from http://github.com/esp8266/Arduino
#include <PubSubClient.h>      // MQTT library from http://github.com/Imroy/pubsubclient
#include <DHT.h>      // DHT library from http://github.com/adafruit/DHT-sensor-library
                      // Written by ladyada, public domain
#include <EEPROM.h>

#define DHTPIN 14     // what pin we're connected to

// Uncomment whatever type you're using!
//#define DHTTYPE DHT11   // DHT 11
#define DHTTYPE DHT22
//#define DHTTYPE DHT21   // DHT 21 (AM2301)

// Initialize DHT sensor for normal 16mhz Arduino
//DHT dht(DHTPIN, DHTTYPE);
// NOTE: For working with a faster chip, like an Arduino Due or Teensy, you
// might need to increase the threshold for cycle counts considered a 1 or 0.
// You can do this by passing a 3rd parameter for this threshold.  It's a bit
// of fiddling to find the right value, but in general the faster the CPU the
// higher the value.  The default for a 16mhz AVR is a value of 6.  For an
// Arduino Due that runs at 84mhz a value of 30 works.
// Example to initialize DHT sensor for ESP8266:
DHT dht(DHTPIN, DHTTYPE, 20);



const char *ssid =   "HUAWEI_P9lite_C433";      // cannot be longer than 32 characters!
const char *pass =   "94aeec3a";      //
long previousMillis = 0;      // Timer loop from http://www.arduino.cc/en/Tutorial/BlinkWithoutDelay
long interval = 5000;        //

unsigned long intervalEEPROM = 60000; // 1 minute interval
unsigned long prvMillisEEPROM = 0;
// IPAddress server(192, 168, 2, 15);    // Update these with values suitable for your network.
WiFiClient wificlient;
PubSubClient client("mqtt.eclipse.org", 1883, wificlient);

int addr = 0; // EEPROM Address

void setup(){
  EEPROM.begin(512);  //Initialize EEPROM

  // Setup console
  Serial.begin(115200);
  delay(10);
  Serial.println();
  Serial.println();
  dht.begin();
  WiFi.begin(ssid, pass);
  int retries = 0;
  while ((WiFi.status() != WL_CONNECTED) && (retries < 10)) {
    retries++;
    delay(500);
    Serial.print(".");
  }
  if (WiFi.status() == WL_CONNECTED) {
    Serial.println("");
    Serial.println("WiFi connected");
  }
client.connect("clientName");
}

void loop(){
  client.loop();
  unsigned long currentMillis = millis();
    if(currentMillis - previousMillis > interval) {
      // Reading temperature or humidity takes about 250 milliseconds!
      // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
      float h = dht.readHumidity();
      // Read temperature as Celsius
      //float t = dht.readTemperature();
      // Read temperature as Fahrenheit
      float f = dht.readTemperature();
        // Check if any reads failed and exit early (to try again).
        if (isnan(h) || isnan(f)) {
          Serial.println("Failed to read from DHT sensor!");
          return;
        } else {
          if (addr < 512 && (currentMillis - prvMillisEEPROM > intervalEEPROM)) {
            EEPROM.write(addr++, (uint8_t)round(h*(100.0/255.0)));
            EEPROM.commit(); // Store data to EEPROM
          }
        }
        
      uint8_t hm = (uint8_t)round(h*(100.0/255.0));
      previousMillis = currentMillis;
      Serial.print("DTH sensor read and transmitted.\nHumidity: ");
      Serial.print(h);
      Serial.print("   (");
      Serial.print(String(hm).c_str());
      Serial.println(")");

      client.publish("asat/lab3/chip_test/temperature", String(f).c_str());
      client.publish("asat/lab3/chip_test/humidity", String(h).c_str());
  }
}